Link do prezentacji: http://slides.com/aalices/webmachine/live#/

Wykorzystujemy aplikację DEMO - przykład z oficjalnego repozytorium Webmachine. Skupiamy się na pliku webmachine_demo_resource.erl, gdzie pokazujemy działanie prostego GET z wykorzystaniem operacji na URL oraz autoryzacji (basic realm). Przykład pozwala również na wyświetlenie diagramu debugującego.

Uruchomienie serwera: ./start.sh